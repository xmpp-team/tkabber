! Main window geometry
Tkabber.geometry:                           788x550-70+100
!Tkabber.geometry:                          180x400-70+100

! Chat window geometry (in no tabs mode)
*Chat.geometry:                             500x500

! Browser and Headlines window geometry (in no tabs mode)
*JBrowser.geometry:                         500x500

! Roster width (tabbed interface only)
Tkabber.mainRosterWidth:                    120

! Roster width in groupchat windows
Tkabber.chatRosterWidth:                    90

! Scrollbar bed color
*troughColor:                               #e3e3e3

*background:                                #f0f0f0
*foreground:                                #000000
*readonlyBackground:                        #f0f0f0
*disabledBackground:                        #f0f0f0
*disabledForeground:                        #999999
*errorForeground:                           firebrick4

! Colors, which are used when mouse is over the item
*activeBackground:                          #c7e1ff
*activeForeground:                          #0000d1

! Colors and border width selected item
*selectBackground:                          #e8f3ff
*selectForeground:                          #0000d1
*selectBorderWidth:                         0
*inactiveSelectBackground:                  #e8f3ff

! Color for checkboxes
*selectColor:                               #c7e1ff
*Menu*selectColor:                          #6b99ff

! Color of traversal highlight rectangle
*highlightBackground:                       #f0f0f0
*highlightColor:                            #6b99ff

! Color of insertion cursor
*insertBackground:                          #000000

! Font for drawing text (except chats and roster font)
*font:                                      {Arial 10}

! Currently there is only progressbar (at the splash screen)
*ProgressBar.foreground:                    #6b99ff

! Flatten ComboBox and ArrowButton
*Spinbox.borderWidth:                       1
*Spinbox.background:                        #f8f8f8
*SpinBox.borderWidth:                       1
*SpinBox*Entry*highlightBackground:         #f8f8f8
*ComboBox.borderWidth:                      1
*ArrowButton.borderWidth:                   0
*ArrowButton.highlightThickness:            0

! Colors for multicolumn listboxes (as in search result window)
*Mclistbox.background:                      #f0f0f0
*Mclistbox.labelActiveBackground:           #c7e1ff
*Mclistbox.labelBackground:                 #e1e1e1
*Mclistbox.labelForeground:                 #000000
*Mclistbox.labelActiveForeground:           #0000d1

*Button.background:                         #e1e1e1
*Menubutton.background:                     #e1e1e1
*Menu.activeBackground:                     #c7e1ff
*Chat*Button.background:                    #f0f0f0
*Chat*Menubutton.background:                #f0f0f0
*Message*Menubutton.background:             #f0f0f0
*bbox.Button.background:                    #e1e1e1
*mainframe.topf.tb0.bbox.Button.background: #f0f0f0
*bottom.buttons1.Button.background:         #f0f0f0

! Type of subitem in roster (1 - display number of resources,
! 2 - display arrow, 3 - display both, 0 - display nothing
*Roster.subitemtype:                        3

! Roster background color
*Roster.cbackground:                        #f8f8f8

! Inactive metaJID background color
*Roster.metajidfill:                        #f4f4f4

! Active metaJID background color
*Roster.metajidhlfill:                      #c7e1ff

! Color of border around metaJID
*Roster.metajidborder:                      #f4f4f4

! Inactive JID background color
*Roster.jidfill:                            #f8f8f8

! Active JID background color
*Roster.jidhlfill:                          #c7e1ff

! Color of border around JID
*Roster.jidborder:                          #f8f8f8

! Inctive group background color
*Roster.groupfill:                          #f0f0f0

! Inactive closed group background color
*Roster.groupcfill:                         #f0f0f0

! Active group background color
*Roster.grouphlfill:                        #c7e1ff

! Color of border around group
*Roster.groupborder:                        #f0f0f0

! Indent of group names
*Roster.groupindent:                        21

! Indent of JIDs
*Roster.jidindent:                          42

! Indent of second order JIDs
! (resources for those who is logged in multiple times)
*Roster.subjidindent:                       62

! Indent of group icons (closed or open arrow icon)
*Roster.groupiconindent:                    2

! Indent of group icons when subitemtype is 2 or 3
*Roster.subgroupiconindent:                 2

! Indent of regular JID icons (status icon)
*Roster.iconindent:                         21

! Indent of second order JID icons
*Roster.subiconindent:                      42

! Additional amount of text height
*Roster.textuppad:                          0
*Roster.textdownpad:                        0

! Vertical distance between adjacent items
*Roster.linepad:                            2

! Foregrounds of JID label
! (stalkerforeground is for contacts with pending subscription)
*Roster.stalkerforeground:                  #663333

! Other colors are selfexplanatory
*Roster.unavailableforeground:              #515151
*Roster.dndforeground:                      #515129
*Roster.xaforeground:                       #0b3760
*Roster.awayforeground:                     #0b3760
*Roster.availableforeground:                dodgerblue4
*Roster.chatforeground:                     dodgerblue4

! Colors in chat and groupchat windows
*Chat*Text*Label.background:                #f8f8f8

! Color of other people nicknames
*Chat.theyforeground:                       firebrick4

! Color of my nickname
*Chat.meforeground:                         dodgerblue4

! Colors of server messages
*Chat.serverlabelforeground:                seagreen
*Chat.serverforeground:                     #4b3a70

! Color of info & error messages
*Chat.infoforeground:                       dodgerblue4
*Chat.errforeground:                        firebrick4

! Color of inactive urls in text
*urlforeground:                             dodgerblue4

! Color of active urls in text
*urlactiveforeground:                       dodgerblue3

! Colors of erroneous words (when ispell module is using)
*Text.errorColor:                           firebrick4
*Text.comboColor:                           dodgerblue4

! Colors of tab labels (when in tabbed mode)
! Usual color
*alertColor0:                               black

! Color when server message is arrived
*alertColor1:                               mediumpurple4

! Color when message is arrived
*alertColor2:                               dodgerblue4

! Color when personally addressed message is arrived
*alertColor3:                               firebrick4

! Colors for browser and discovery service windows
*JBrowser.fill:                             #000000
*JBrowser.activefill:                       #000000
*JBrowser.border:                           #f8f8f8
*JBrowser.nscolor:                          #2f6099
*JBrowser.nsactivecolor:                    #2f6099

*JDisco.fill:                               #000000
*JDisco.activefill:                         #000000
*JDisco.border:                             #f8f8f8
*JDisco.featurecolor:                       #2f6099
*JDisco.identitycolor:                      DarkGreen
*JDisco.optioncolor:                        DarkViolet
*Tree*background:                           #f8f8f8
*linesfill:                                 #000000
*crossfill:                                 #000000

*Customize.varforeground:                   dodgerblue4

! Tooltip options
*Balloon*background:                        #fffae6
*Balloon*foreground:                        #000000
*DynamicHelp.background:                    #fffae6
*DynamicHelp.foreground:                    #000000


*Listbox.background:                        #f8f8f8
*Listbox.foreground:                        #000000
*Listbox.borderWidth:                       1

*Text.background:                           #f8f8f8
*Text.foreground:                           #000000
*Text.borderWidth:                          1

*Entry.background:                          #f8f8f8
*Entry.foreground:                          #000000
*Entry.borderWidth:                         1

*NoteBook*Entry.background:                 #f8f8f8
*NoteBook*Entry.disabledBackground:         #f0f0f0
*NoteBook*Entry.foreground:                 #000000

*Menu.activeBorderWidth:                    1
*Menu.borderWidth:                          1
*Menubutton.borderWidth:                    1

*ButtonBox*borderWidth:                     0
*ButtonBox*activeBorderWidth:               0

*Button.borderWidth:                        1

! vim:ft=xdefaults:ts=8:sw=4:sts=4:et
