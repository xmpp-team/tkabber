# default.tcl --
#
#       This file is a part of the Tkabber XMPP client. It defines fonts for
#       rosters and chatlog windows, and provides a few useful bindings.

if {$::tcl_platform(platform) eq "unix" && \
        ([catch {tk::pkgconfig get fontsystem} fontsystem] || \
         $fontsystem ne "xft")} {
    set XLFDFonts 1
} else {
    set XLFDFonts 0
}

proc create_default_fonts {class} {
    global XLFDFonts
    global ${class}Font ${class}BoldFont ${class}ItalicFont \
           ${class}BoldItalicFont

    # Getting the default font in text widget
    set f [frame .tmpframe -class $class]
    set t [text $f.tmptext]
    set ${class}Font [lindex [$t configure -font] 4]
    font create ${class}Font {*}[font actual [set ${class}Font]]
    set ${class}BoldFont [option get .tmpframe boldFont $class]
    set ${class}ItalicFont [option get .tmpframe italicFont $class]
    set ${class}BoldItalicFont [option get .tmpframe boldItalicFont $class]
    destroy $f

    if {!$XLFDFonts} {
        set ::default_${class}_font \
            [list [font actual ${class}Font -family] \
                  [font actual ${class}Font -size]]
        set ${class}Font ${class}Font
    }

    if {!$XLFDFonts || [set ${class}BoldFont] eq ""} {
        set ${class}BoldFont \
            [font create ${class}BoldFont {*}[font configure ${class}Font] \
                         -weight bold]
    }
    if {!$XLFDFonts || [set ${class}ItalicFont] eq ""} {
        set ${class}ItalicFont \
            [font create ${class}ItalicFont {*}[font configure ${class}Font] \
                         -slant italic]
    }
    if {!$XLFDFonts || [set ${class}BoldItalicFont] eq ""} {
        set ${class}BoldItalicFont \
            [font create ${class}BoldItalicFont \
                         {*}[font configure ${class}Font] \
                         -weight bold -slant italic]
    }
}

proc redefine_fonts {class args} {
    font configure ${class}Font {*}$args
    font configure ${class}BoldFont {*}[font configure ${class}Font] \
                   -weight bold
    font configure ${class}ItalicFont {*}[font configure ${class}Font] \
                   -slant italic
    font configure ${class}BoldItalicFont {*}[font configure ${class}Font] \
                   -weight bold -slant italic
}

create_default_fonts Chat
create_default_fonts Roster

if {![info exists usetabbar]} {
    set usetabbar 1
}

bind Text <<ScrollUp>> {
    %W yview scroll -5 units
}
bind Text <<ScrollDown>> {
    %W yview scroll 5 units
}
bind Listbox <<ScrollUp>> {
    %W yview scroll -5 units
}
bind Listbox <<ScrollDown>> {
    %W yview scroll 5 units
}
bind Treeview <<ScrollUp>> {
    %W yview scroll -5 units
}
bind Treeview <<ScrollDown>> {
    %W yview scroll 5 units
}

bind Text <<ScrollLeft>> {
    %W xview scroll -10 units
}
bind Text <<ScrollRight>> {
    %W xview scroll 10 units
}
bind Listbox <<ScrollLeft>> {
    %W xview scroll -10 units
}
bind Listbox <<ScrollRight>> {
    %W xview scroll 10 units
}
bind Treeview <<ScrollLeft>> {
    %W xview scroll -10 units
}
bind Treeview <<ScrollRight>> {
    %W xview scroll 10 units
}

if {([catch {tk windowingsystem}] && $::tcl_platform(platform) eq "unix") ||
        (![catch {tk windowingsystem}] && [tk windowingsystem] eq "x11")} {
    event add <<ScrollUp>>    <4>
    event add <<ScrollDown>>  <5>
    event add <<ScrollLeft>>  <Shift-4>
    event add <<ScrollRight>> <Shift-5>
}

if {$::tcl_platform(platform) eq "windows"} {
    # workaround for shortcuts in russian keyboard layout
    event add <<Cut>>   <Control-division>
    event add <<Copy>>  <Control-ntilde>
    event add <<Paste>> <Control-igrave>
    event add <<Undo>>  <Control-ydiaeresis>
    event add <<Redo>>  <Control-ssharp>
    event add <<CollapseRoster>>  <Control-ecircumflex>
    event add <<OpenSearchPanel>> <Control-ucircumflex>
}

if {[event info <<ContextMenu>>] eq ""} {
    # tk < 8.6
    if {$::aquaP} {
        # workaround for tk < 8.6.1
        set context 2
    } else {
        set context 3
    }
    event add <<ContextMenu>> <$context>
}
if {[event info <<PasteSelection>>] eq ""} {
    if {$::aquaP} {
        set middle 2
    } else {
        set middle 3
    }
    event add <<PasteSelection>> <$middle>
}

# vim:ft=tcl:ts=8:sw=4:sts=4:et
