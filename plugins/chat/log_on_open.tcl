# log_on_open.tcl --
#
#       This file is a part of the Tkabber XMPP client. It's a plugin which
#       displays several previously received messages on opening a chat
#       window. The maximum number of messages and time interval for
#       messages too old to show are customizable.

namespace eval log_on_open {
    custom::defvar options(enable_in_chats) 1 \
        [::msgcat::mc "Show several most recently received messages in a\
                       newly opened chat window. They are taken from the\
                       logs, so you'll have to enable them for chats."] \
        -type boolean -group Chat
    custom::defvar options(enable_in_groupchats) 0 \
        [::msgcat::mc "Show several most recently received messages in a\
                       newly opened conference window. They are taken from\
                       the logs, so you'll have to enable them for\
                       groupchat."] \
        -type boolean -group Chat
    custom::defvar options(max_messages) 20 \
        [::msgcat::mc "Maximum number of log messages to show in newly\
                       opened chat window (if set to negative then the\
                       number is unlimited)."] \
        -type integer -group Chat
    custom::defvar options(max_interval) 24 \
        [::msgcat::mc "Maximum interval length in hours for which log\
                       messages should be shown in newly opened chat\
                       window (if set to negative then the interval is\
                       unlimited)."] \
        -type integer -group Chat
    custom::defvar options(use_metacontacts) 0 \
        [::msgcat::mc "If the chat window that is being opened belongs\
                       to a JID which corresponds to some metacontact\
                       in your roster then messages from all JIDs from\
                       that metacontact will show up in the window.\
                       This option requires enabling metacontacts in your\
                       roster."] \
        -type boolean -group Chat
}

proc log_on_open::show {chatid type} {
    variable options

    if {$type ne "chat" && $type ne "groupchat"} return
    if {$type eq "chat" && !$options(enable_in_chats)} return
    if {$type eq "groupchat" && !$options(enable_in_groupchats)} return

    set xlib [chat::get_xlib $chatid]
    set jid [chat::get_jid $chatid]
    set bare_jid [::xmpp::jid::removeResource $jid]
    set gc [chat::is_groupchat [chat::chatid $xlib $bare_jid]]

    if {!$gc && (![::logger::logs_exist $jid] || \
                 [::xmpp::jid::equal $jid $bare_jid])} {
        set log_jids [list $bare_jid]
        if {$::ifacetk::roster::options(enable_metacontacts) && \
                [llength [info procs ::plugins::metacontacts::*]] > 0 && \
                $options(use_metacontacts)} {
            foreach tag [::plugins::metacontacts::get_all_tags $xlib] {
                set jids [::plugins::metacontacts::get_jids $xlib $tag]
                foreach j $jids {
                    if {[::xmpp::jid::equal $j $bare_jid]} {
                        set log_jids $jids
                        break
                    }
                }
            }
        }
    } else {
        set log_jids [list $jid]
    }

    set messages [::logger::get_last_messages $log_jids $type \
                                              $options(max_messages) \
                                              $options(max_interval)]

    foreach msg $messages {
        array unset tmp
        if {[catch {array set tmp $msg}]} continue

        set x {}
        if {[info exists tmp(timestamp)]} {
            set seconds [clock scan $tmp(timestamp) -gmt 1]
            lappend x [::xmpp::delay::create $seconds]
        }
        if {[info exists tmp(jid)]} {
            if {$tmp(jid) == ""} {
                # Synthesized message
                set from ""
            } elseif {(!$gc && [::xmpp::jid::removeResource $tmp(jid)] != $bare_jid) || \
                 $gc && $tmp(jid) != $jid} {
                set from $tmp(jid)
            } else {
                set from $jid
            }
        } else {
            set from ""
        }
        # Don't log this message. Request this by creating very special 'empty'
        # tag which can't be received from the peer.
        # TODO: Create more elegant mechanism
        lappend x [::xmpp::xml::create "" -xmlns tkabber:x:nolog]

        chat::add_message $chatid $from $type $tmp(body) $x
    }
}

hook::add open_chat_post_hook [namespace current]::log_on_open::show 100

# vim:ft=tcl:ts=8:sw=4:sts=4:et
